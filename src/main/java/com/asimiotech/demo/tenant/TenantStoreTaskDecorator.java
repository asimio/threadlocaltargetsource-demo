package com.asimiotech.demo.tenant;

import lombok.RequiredArgsConstructor;
import org.springframework.core.task.TaskDecorator;

@RequiredArgsConstructor
public class TenantStoreTaskDecorator implements TaskDecorator {

    private final TenantStore tenantStore;

    @Override
    public Runnable decorate(Runnable task) {
        String tenantId = this.tenantStore.getTenantId();
        return () -> {
            try {
                this.tenantStore.setTenantId(tenantId);
                task.run();
            } finally {
                this.tenantStore.setTenantId(null);
            }
        };
    }

}