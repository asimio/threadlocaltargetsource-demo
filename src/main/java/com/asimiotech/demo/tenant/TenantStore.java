package com.asimiotech.demo.tenant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TenantStore {

	private String tenantId;

	public void clear() {
	    this.tenantId = null;
	}

}